import java.util.StringTokenizer;
import java.io.*;
import java.util.Vector;
import java.net.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.auth.*;
import org.apache.commons.httpclient.methods.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

//Get at String parameters and discover which are projects
import hudson.model.*
import hudson.util.*

def g_currentBuild = Thread.currentThread().executable;
def g_currentProject =  g_currentBuild.getProject();
def envVars = g_currentBuild.properties.get("envVars");

println("");
println("---------------------------------------------------");
println("------ Read build parameters from JOB - Start -----");
println("---------------------------------------------------");

String strUsername;
String strPassword;
String sJenkinsURL = envVars["JENKINS_URL"];
	
// Detect which Jenkins environment we are in.
if (sJenkinsURL != null) {	
	if (sJenkinsURL.contains("hcntpnv03")) {
		strHostname = "hcntpnv03";
		strUsername = "_HPOC_RND_IMP";
		strPassword = "EBA_SB_1112";
	} 
	else if (sJenkinsURL.contains("hcntpnv02")) {
		strHostname = "hcntpnv02";
		strUsername = "_HUDSON_RC_DEV";
		strPassword = "EBA_CI_1112";
	} 
	else if (sJenkinsURL.contains("hcntpn01")) {
		strHostname = "hcntpn01";
		strUsername = "_HUDSON_RC_PROD";
		strPassword = "rnd_EBA_Rc_235498";
	} 
	else if (sJenkinsURL.contains("build.paychex.com")) {
		strHostname = "build.paychex.com";
		strUsername = "_HUDSON_RC_PROD";
		strPassword = "rnd_EBA_Rc_235498";
	} 
}

// Get job information
String g_sJobName = g_currentBuild.getParent().getName();
String g_sJenkinsToolsFolder = "n:\\Jenkins\\tools\\";
String g_sStagingTempFolder = g_sJenkinsToolsFolder + "Backup\\Staging\\";

println("\nProcessing Artifacts...");

// Retrieve all supported environment variables.
String deployEnvVar = g_currentBuild.buildVariables.get("DeployEnvironment");
String envVar = g_currentBuild.buildVariables.get("Environment");
String deploymentEnvVar = g_currentBuild.buildVariables.get("DeploymentEnvironment");
String stagingEnvVar = g_currentBuild.buildVariables.get("StagingEnvironment");

AbstractProject proj = g_currentBuild.getProject();
String g_sJobName_Staging = proj.getName();
String g_sStagingJobTempFolder = g_sStagingTempFolder + g_sJobName_Staging;

// Delete the old temporary folder.
deleteFileOrFolder(new File(g_sStagingJobTempFolder));

// Create the new temporary folder.
File jobDir = new File(g_sStagingJobTempFolder);
jobDir.mkdir();

// *** Start - Set the Staging build to be kept forever IF we are staging to production.
// Get the correct deployment variable
if (deployEnvVar == "") {deployEnvVar = envVar;}
// Convert to lowercase.
deployEnvVar = deployEnvVar.toLowerCase();
// See if we are staging to production.
if (deployEnvVar.indexOf("prod") != -1) {
	g_currentBuild.keepLog(true);
	println("  *** Production stage detected: [" + deployEnvVar + "] - Keep Build Forever ***");	
} else {
	println("  *** Non-Production stage detected: [" + deployEnvVar + "] ***");		
}
// *** End - Set the Staging build to be kept forever IF we are staging to production.

//fill descriptionDataStrings Vector with the string parameters
Vector<String> paramNames = new Vector<String>();
// Description data to be retrieved from the job config.xml.
List<String> descriptionDataStrings = new ArrayList<String>();

Job[] subJobsArr = proj.getAllJobs().toArray();

if (subJobsArr.length > 0)  {

		Job thisJob = subJobsArr[0];
    Run desiredBuild = thisJob.getLastBuild();
    
    // Get build parameters framework from our current build.
		def dataItems = desiredBuild.getActions(hudson.model.ParametersAction);

		// From the first dataItems object, get the list of parameters.
		Object[] paramDefs = dataItems.get(0).getParameters().toArray();
		
		// We need at least one of these objects to retrieve params from job.
		if (dataItems.size() == 0)
		{
			println("!!! ERROR: Warning: No job parameters found !!!");
		}

		// Fill the descriptionDataStrings with the desription data from the build xml (string parameters only).
		getDescriptionDataFromJob(descriptionDataStrings, strHostname, envVars["JOB_NAME"], strUsername, strPassword);

  	//println("  *** dataItemsProj.size(): [" + dataItemsProj.size() + "] ***");		
		//println("  *** paramDefsProj.length: [" + paramDefsProj.length + "] ***");		

		println("   - Get job parameters from Jenkins model -");
		int iStringParamCount = 0;
		// Loop through all of the parameters and grab the String ones.
    for (int i = 0; i < paramDefs.length; i++) {
        
        def defParamVal = paramDefs[i];

        if (defParamVal != null && defParamVal instanceof hudson.model.StringParameterValue) {      
        	  	
        	String sDescription = paramDefs[i].getDescription();
        	String sName = paramDefs[i].getName();
        	String sValue = paramDefs[i].getValue();
        	
        	if (sName != "DeployEnvironment") {
        		iStringParamCount++;	
	        	paramNames.add(sName);	        	
					} else {
						println("   --- Skipping DeployEnvironment variable.  class:[" + paramDefs[i].getClass() + "]")	
					}
       	}        	
     }
  }

// Get the size of both arrays
int iParamName_Count = paramNames.size();
int iParamDescription_Count = descriptionDataStrings.size();

println("   --- Param sizes: Names(from model)[" + iParamName_Count + "] Descriptions(from config.xml)[" + iParamDescription_Count + "]");
// Verify that both the param names and the param description counts are the same.
//   ...otherwise we are out of sync and should fail the build.
if (iParamName_Count != iParamDescription_Count) {
	println("-- Parameter mismatch -- OK -"); 
	
  // Display the parameters retrieved for debugging.
  for (int ii = 0; ii < paramNames.size(); ii++) {
				// For debug.
			println("paramNames.get(" + ii + ") = " + paramNames.get(ii));
			if (ii < descriptionDataStrings.size()) {
				println("descriptionDataStrings.get(" + ii + ") = " + descriptionDataStrings.get(ii));
			} else {
				println("descriptionDataStrings.get(" + ii + ") = [No match]");
			}
			println("--------");
  }
  println("");
}  
  
println("   - Parse parameter description data -");

//Iterate over all job String Param description fields and parse them.
for (int iStringParamDescription_Index = 0; iStringParamDescription_Index < descriptionDataStrings.size(); iStringParamDescription_Index++) {
	
	//println("iStringParamDescription_Index: [" + iStringParamDescription_Index + "]");
	String dataStringElt = descriptionDataStrings.get(iStringParamDescription_Index);
	//println("dataStringElt: [" + dataStringElt + "]");

	if (dataStringElt == null)
	{
		println("   - !!!! dataStringElt is NULL !!!! -");
	} else {
		//println("   - dataStringElt size: [" + dataStringElt.size() + "]");
	}

	//println("   - dataStringElt: [" + dataStringElt + "]");

	//Get the RC job name out of the string
	if(dataStringElt.indexOf("/job/") > 0) {	
		int indexOfJobName = dataStringElt.indexOf("/job/") + "/job/".length();
		int endIndexOfJobName = dataStringElt.indexOf("/", indexOfJobName+1);
		String jobName = dataStringElt.substring(indexOfJobName, endIndexOfJobName);

		// Using the index of the string param description field, get the associated parameter name.
		String buildNum = g_currentBuild.buildVariables.get(paramNames.get(iStringParamDescription_Index));
		// NOTE: Do we still need to use the _build parameter?
		String buildNumBuild = g_currentBuild.buildVariables.get(paramNames.get(iStringParamDescription_Index) + "_build");
		if (buildNumBuild != null) {
			buildNum = buildNumBuild;
		}

		int iArtifactCount = 0;
		int StartIndexOfArtifactLocation = dataStringElt.indexOf("Artifact location:") + "Artifact location:".length();

		// Print the job name to the log.  More log information will follow.
		if (buildNum.equals("0") | buildNum.equals("")) {
			println("\n" + jobName + " --- SKIPPED - Build number:[0, or blank] was specified - OK.");
			StartIndexOfArtifactLocation = 0;
		} else {// If the build number is non-zero - print the name
			println("\n jobName = " + jobName);
		}


		/* ---------------------------------------------------------------------------------------------------
			Note: For each description field, there can be multiple artifact locations separated by commas
			Loop through those here and process.
		----------------------------------------------------------------------------------------------------*/
		//Get the Artifact location, might be bolded and might not
		while (StartIndexOfArtifactLocation > "Artifact location:".length()) {

			// Skip over the bold html code.
			if (dataStringElt.substring(StartIndexOfArtifactLocation).startsWith("</b>")) {
				StartIndexOfArtifactLocation += 4;
			}
			
			// For each Artifact location string, check for and ending comma or carriage return.
			int endIndexOfArtifactLocation = dataStringElt.indexOf(",", StartIndexOfArtifactLocation);
			if (endIndexOfArtifactLocation < 0) {
				//endIndexOfArtifactLocation = dataStringElt.indexOf("\n", StartIndexOfArtifactLocation);
				endIndexOfArtifactLocation = dataStringElt.indexOf("<br>", StartIndexOfArtifactLocation);
			}
			
			// Get the actual artifact location string.
			//println("StartIndexOfArtifactLocation: [" + StartIndexOfArtifactLocation + "]");
			//println("endIndexOfArtifactLocation: [" + endIndexOfArtifactLocation + "]");
			String artifactPattern = dataStringElt.substring(StartIndexOfArtifactLocation, endIndexOfArtifactLocation).trim();
			println("    Source: [" + artifactPattern + "]");
			iArtifactCount++;

			// For each Location, we now get the Artifact destination, might be bolded and might not		
			int startIndexOfTargetDirectory = dataStringElt.indexOf("Target Directory:", endIndexOfArtifactLocation) + "Target Directory:".length();

			// Skip over the bold html code.
			if (dataStringElt.substring(startIndexOfTargetDirectory).startsWith("</b>")) {
				startIndexOfTargetDirectory += 4;
			}
			
			// Find the end of the target directory string.  It can be a carriage return or a <br.
			int endIndexOfTargetDirectory = dataStringElt.indexOf("<br", startIndexOfTargetDirectory);
			int endIndexOfTargetDirectory2 = dataStringElt.indexOf("\n", startIndexOfTargetDirectory);
			if (endIndexOfTargetDirectory2 < endIndexOfTargetDirectory && endIndexOfTargetDirectory2 > 0) endIndexOfTargetDirectory = endIndexOfTargetDirectory2;
			if (endIndexOfTargetDirectory < 0) endIndexOfTargetDirectory = dataStringElt.length();

			// Grab the actual target directory string.
			String targetDir = dataStringElt.substring(startIndexOfTargetDirectory, endIndexOfTargetDirectory).trim();
			
			// Search and replace the deploy environment variable.
			// Ask Gonzo
			//println("targetDir = " + targetDir);
			//println("deployEnvVar = " + deployEnvVar);
			if (deployEnvVar != null) {
				targetDir = targetDir.replace("%DeployEnvironment%", deployEnvVar);
				//println("targetDir2 = " + targetDir);
				targetDir = targetDir.replace("DeployEnvironment", deployEnvVar);
				//println("targetDir3 = " + targetDir);
			}

			if (envVar != null) {
					targetDir = targetDir.replace("%Environment%", envVar);
					targetDir = targetDir.replace("Environment", envVar);
			}

			println("    Target: [" + targetDir + "]");

		// adf 5/1/2012 - Allow the build numbner to contain the informational string "DOES NOT EXIST" for a default value.  
		// This will be converted to a "0" to simply ignor the job.
		if(buildNum.equals("DOES NOT EXIST")) {buildNum = "0";}

		// adf 5/2/2012 - Add display of buiuld number.
			println("    Build Number: [" + buildNum + "]");

			// If the build number not zero, we process the parameters.  If zero, we skip. 
			if (!buildNum.equals("0")) {
				
				// Find and replace the supported environment variables in the source string.
				if (deployEnvVar != null) {
					artifactPattern = artifactPattern.replace("%DeployEnvironment%", deployEnvVar);
					artifactPattern = artifactPattern.replace("DeployEnvironment", deployEnvVar);
				}
				if (envVar != null) {
					artifactPattern = artifactPattern.replace("%Environment%", envVar);
					artifactPattern = artifactPattern.replace("Environment", envVar);
				}
				if (deploymentEnvVar != null) {
					artifactPattern = artifactPattern.replace("%DeploymentEnvironment%", deploymentEnvVar);
					artifactPattern = artifactPattern.replace("DeploymentEnvironment", deploymentEnvVar);
				}
				if (stagingEnvVar != null) {
					artifactPattern = artifactPattern.replace("%StagingEnvironment%", stagingEnvVar);
					artifactPattern = artifactPattern.replace("StagingEnvironment", stagingEnvVar);
				}
				artifactPattern = artifactPattern.replace("\$", "");

				// ******************************************************
				// Get the actual artifacts from the RC job.
				// ******************************************************
				GetArtifactsFromRCJob(jobName, buildNum, artifactPattern, true, targetDir, g_sJobName_Staging, g_sStagingJobTempFolder, g_currentBuild, g_sStagingTempFolder);
			} 
			else // The build number IS 0 - this means we skip processing.
			{
				println("    --- SKIPPED:[" + jobName + "] - Build Number [" + buildNum +"] - Zero Specified - OK ---");
				
				// If a build number of 0 is specified, we can jump out of the loop.  
				// The eliminates clutter in the console window.
				StartIndexOfArtifactLocation = -1;
				continue;
			}

			//Repeat to handle multiple lines. New line can be <br> or \n		
			int StartIndexOfArtifactLocation2 = dataStringElt.indexOf("Artifact location:", endIndexOfTargetDirectory) + "Artifact location:".length();
			
			if (StartIndexOfArtifactLocation2 < StartIndexOfArtifactLocation) 
				{StartIndexOfArtifactLocation = -1;}
			else																															
				{StartIndexOfArtifactLocation = StartIndexOfArtifactLocation2;}
		}
		if (iArtifactCount == 0)
		{
			if (buildNum != "0"){ println("    --- SKIPPED: No Artifacts defined ---");}
		}
	}
}

println("");
println("---------------------------------------------------");
println("------ Read build parameters from JOB - End -------");
println("---------------------------------------------------");
println("");

/* ------------------------------------------------------------------------- 
	Deletes a speicified file or folder from the system.
------------------------------------------------------------------------- */
void deleteFileOrFolder(File f) throws IOException {
  if (f.isDirectory()) {
    for (File c : f.listFiles())
      deleteFileOrFolder(c);
  }
  //println("Delete " + f);
  f.delete()
//  if (!f.delete())
//    throw new FileNotFoundException("Failed to delete file: " + f);
}

/* ------------------------------------------------------------------------- 
	Process the source string to enable it to properly handle wildcards...
		- Convert single to double backslashes 
		- Convert forward slashed to double backslashes
		- Convert * to .* (regex compatable)
------------------------------------------------------------------------- */
public boolean wildCardCompare(String fileName, String wildCard) {
	wildCard = wildCard.replace('\\','\\\\');
	wildCard = wildCard.replace("/", "\\\\");
	wildCard = wildCard.replace("*", ".*");
	
	// Adding a wildcard to the front of the string (??)
	wildCard = ".*" + wildCard;

	return fileName.matches(wildCard);
}

/* -----------------------------------------------------------------
	GetArtifactsFromRCJob()

	Get artifacts from a specified RC job
	Note: Because groovy does not handle global variables, 
						- everything must be passed in to this function 
----------------------------------------------------------------- */
public void GetArtifactsFromRCJob(String sJobName_RC, String buildNumber, String artifactPattern, boolean bFlattenSourcePath, String targetDirectory, String sJobName_Staging, String sStagingJobTempFolder, AbstractBuild currentBuild, String sStagingTempFolder) 
{	
	String sSourceFullPath;
	String sSourceRelativePath;
	String sSourceFileName;
	String sDestinationFullPath;
	String sWorkspaceFolder = currentBuild.getWorkspace();
	String sAgent = currentBuild.getBuiltOnStr();
	String debugBuildEnvVar = currentBuild.buildVariables.get("DEBUG_stage_ONLY");

	if (sJobName_Staging == null) {
		println("*** Job Name not specified ***" + sJobName_RC);
		return;
	}
	
	String sToolsFolder = sStagingJobTempFolder + "\\"
 
	// Get the job.
	def item = Hudson.instance.getItem(sJobName_RC);
	
	if (item == null) {
		println("*** Specified job not found in Jenkins  [" + sJobName_RC + "] ***" );
		return;
	}
	
	Job[] subJobsArr = item.getAllJobs().toArray();

	if (subJobsArr.length > 0)  {
		for (int ii = 0; ii < subJobsArr.length; ii++){
			Job thisJob = subJobsArr[ii];
			String sThisJob = thisJob;
			//If the job contains "relativePath" then it is a subcomponent of the parent job and is not needed for this process, so skip it
			if (!sThisJob.contains("relativePath")) {
				//println ("        subArray:" + ii + " Root Job: USE:thisJob: \n            " + thisJob );
				// Find the specific build number if not specified.
				Run desiredBuild = null;
				// the passed in build number could be an iteger or alpha.  If it is alpha, check for latest build		
				// For build number 'L' get the latest build.
				if(buildNumber.equals("L") || buildNumber.equals("l") || buildNumber.toLowerCase().equals("latest")) {
					desiredBuild = thisJob.getLastBuild();
					
					if (desiredBuild != null) {
						println("    Latest Build Number of " + sJobName_RC + " is " + desiredBuild.number);
						buildNumber = "" + desiredBuild.number;
					} else {
						println("    No last build for " + sJobName_RC);
					}
				} else {
					desiredBuild = thisJob.getBuildByNumber(Integer.parseInt(buildNumber)) ;
				}
				////////////////////////////////////////////////////////////////////////////
				
				// Build number not found.
				if (desiredBuild == null)  {
					println("       !!! BUILD NUMBER [" + buildNumber + "] NOT FOUND FOR PROJECT [" + sJobName_RC + "] !!!");
				} else {
					// Add the staging job link to the RC job description if it is not there.
					String descriptionString = thisJob.getDescription();
					descriptionString = removeBadLinks(descriptionString, thisJob);
					if (!descriptionString.contains("Staging Job for " + thisJob.getName() + ": " + sJobName_Staging)) {
						println("Warning: " + thisJob.getName() + " does not have a staging job assigned - Adding now.");
						println("");
						thisJob.setDescription(descriptionString 
															+ "\n<br><br><a href=\"http://build.paychex.com:8080/view/Staging/view/All/job/" 
															+ sJobName_Staging 
															+ "\" target=\"_blank\">\nStaging Job for " + thisJob.getName() + ": " 
															+ sJobName_Staging 
															+ "</a>"
															);
						thisJob.save();
					}
			
					// Check the status.
					// Currently, we do not do anything if a specified RC build has not succeeded.
					// Maybe in the future?
					if (desiredBuild.result == Result.UNSTABLE) {}
					if (desiredBuild.result == Result.SUCCESS) {}
					if (desiredBuild.result == Result.FAILURE) {}
							
					// Get the artifact list from the speicfied build.
					List<Run.Artifact> artifacts = desiredBuild.getArtifacts();
					Run.Artifact[] artifactArr = artifacts.toArray();
						
					boolean bMatchFound = false;
					
					// Provide the ability to supply a prioritized, semi-colon delimited string for the artifact source location
					String[] prioritizedArtfiactPatternArray = artifactPattern.split( /;/ );
					for( int artifactPatternIndex = 0; artifactPatternIndex < prioritizedArtfiactPatternArray.size(); artifactPatternIndex++ ) {

						String prioritizedArtfiactPattern = prioritizedArtfiactPatternArray[ artifactPatternIndex ];
						
						// If we've had an artifact pattern match (bMatchFound == true), we don't look further in the list pattern array.
						if( bMatchFound == true ) {
							break;
						}					
					
						// Find artifact that match the specified suffix.
						for (int i = 0; i < artifactArr.length; i++)  {
							// Get the file name of the artifact
							sSourceFileName = artifactArr[i].getFileName();
				
							// Asssemble the full artifact source path.
							sSourceRelativePath = artifactArr[i].relativePath;
							sSourceFullPath = (desiredBuild.getArtifactsDir()).getAbsolutePath();
							sSourceFullPath = sSourceFullPath + "\\" + sSourceRelativePath;
							sSourceFullPath = sSourceFullPath.replace('/','\\');
							sSourceFullPath = sSourceFullPath.replace('\"','');
			
							// See if the extension matches what is specified.        	
							if (wildCardCompare(sSourceFullPath, prioritizedArtfiactPattern)) {
								bMatchFound = true;
								
								// If a prioritized list is provided for the source artifact, we should indicate which one in the list we've found:
								if( prioritizedArtfiactPatternArray.size() > 1 ) {
									println("    Source Pattern #" + (artifactPatternIndex + 1).toString() + " Found : [" + prioritizedArtfiactPattern + "]");
								}  							

								sDestinationFullPath = sStagingTempFolder + sJobName_Staging + "\\" + targetDirectory;
								if (!sDestinationFullPath.endsWith("\\") && !sDestinationFullPath.endsWith("/")) {
									sDestinationFullPath = sDestinationFullPath + "\\";
								}
				
								// Determine the destination path.								
								if (bFlattenSourcePath) {
									sDestinationFullPath = sDestinationFullPath + sSourceFileName;
								} else {
									sDestinationFullPath = sDestinationFullPath + sSourceRelativePath;
				
									File folderDest = new File(sDestinationFullPath);
									folderDest.mkdir();
								}
				
								// Remove the filename from the destination path.
								sDestinationFullPath = sDestinationFullPath.replace(sSourceFileName,"");
												
								// Change all slashes to Windows format.
								sDestinationFullPath = sDestinationFullPath.replace('/','\\');
								sDestinationFullPath = sDestinationFullPath.replace('\"','');
								
								def command = "XCOPY /Y /S /Q \"" + sSourceFullPath + "\" \"" + sDestinationFullPath.replace("/", "\\") + "\"";
								
								// Show the copy command for debug buiilds.
								if (debugBuildEnvVar != null && debugBuildEnvVar == "true")	
								{
									println("                   *****************************");	
									println("                   *** For debug builds only - Start ***");	
									println("                   Copy Command:[" + command + "]");	
									println("                   *** For debug builds only - End ***");	
									println("                   *****************************");	
								}
								
								//Execute the command
								def proc = command.execute();
								proc.waitFor();
				
								returnCode = "${ proc.exitValue()}";
								String stderrText = "${proc.err.text}"
								String stdoutText = "${proc.in.text}"
								//println(stderrText);
								//println(stdoutText);
				
								if (returnCode.trim().equals("0")) {
									// The copy Succeeded
									int indexFiles = stdoutText.indexOf("File(s)");
				
									String numFiles = "CCC";
									if (indexFiles > 0)  {
										numFiles = stdoutText.substring(0, indexFiles-1).trim();
									}
									// **NOTE** Do not modofy the following print statement.  It is used in the Deployment TimeLine setter job to extract build information for the staging run
									println("                   *****************************");	
									println("                  *** COPIED " + numFiles + " artifact(s) from [" + sJobName_RC + "] Build Number [" + buildNumber + "]: " + sSourceFileName + " ***");
									println("                   *****************************");	
									//END **NOTE** 
									
									// ***
									// Start - Set the build to be kept forever IF we are staging to production.
									// ***
									def g_currentBuild = Thread.currentThread().executable;
									String deployEnvVar = g_currentBuild.buildVariables.get("DeployEnvironment");
									String envVar = g_currentBuild.buildVariables.get("Environment");
									//String deploymentEnvVar = g_currentBuild.buildVariables.get("DeploymentEnvironment");
									//String stagingEnvVar = g_currentBuild.buildVariables.get("StagingEnvironment");

									// Get the correct deployment variable
									if (deployEnvVar == "") {deployEnvVar = envVar;}
									// Convert to lowercase.
									deployEnvVar = deployEnvVar.toLowerCase();
									// See if we are staging to production.
									if (deployEnvVar.indexOf("prod") != -1) {
										desiredBuild.keepLog(true);
										println("                      *** Stage to Production - Set RC job to: Keep Build Forever ***");	
									}
									// ***
									// End - Set the build to be kept forever IF we are staging to production.
									// ***
									
								} else {
									// The copy Failed
									println("                   *****************************");	
									println("                  !!! NO ARTIFACTS FOUND [" + sJobName_RC + "] Build Number [" + buildNumber + "]: " + sSourceFileName + " !!!");
									println("                   *****************************");	
								}
							}
						}
					}
					/* adf - 2012_05_21 - added check to make sure we actually found artifacts */
					if (bMatchFound == false) {
						println("           !!! NO ARTIFACTS FOUND MATCHING SPECIFIED PATTERN: [" + artifactPattern + "] !!!");
						/* If we get here, we may need to exit and fail - currenly, we just continue */
					}
					
					/* adf - 2012_05_21 - added check in case artifact array is empty */
					if (artifactArr.length == 0) {
						println("           !!!! NO ARTIFACTS FOUND [" + sJobName_RC + "] Build Number [" + buildNumber + "]: " + sSourceFileName + " !!!!");
					}
				}
			} else {
				//println ("        subArray:" + ii + " : Subproject-may be Maven. SKIP:thisJob: \n            " + thisJob );
			}
		}
	}
}

private String removeBadLinks(String descriptionString, Job thisJob) {
	int MAX_ITERATIONS = 10;
	int currentIteration = 0;
	
	String returnString = descriptionString;
	
	int startIndex = descriptionString.indexOf("/job/");
	//println("descriptionString = " + descriptionString);
	
	boolean descChanged = false;
	while (startIndex != -1 && currentIteration < MAX_ITERATIONS) {
		int nameIndex = startIndex + "/job/".length();
		int endIndex = descriptionString.indexOf("\"", nameIndex);
		
		// adf - check for descriptions that do not have staging or rc jobs for whatever reason.
		if (nameIndex == -1 || endIndex == -1) break;
		
		String stagingJobName = descriptionString.substring(nameIndex, endIndex);
		//println("job is (" + stagingJobName + ")");
		def item = Hudson.instance.getItem(stagingJobName);
		if (item == null) {
			println("Staging job link is dead and will be deleted: " + stagingJobName);
			int delStartIndex = returnString.indexOf(stagingJobName);
			delStartIndex = returnString.lastIndexOf("<a href", delStartIndex);
			delStartIndex -= 8; // account for the <br> and href text
			int delEndIndex = returnString.indexOf("</a>", delStartIndex) + 4;
			//println("delete this: " + returnString.substring(delStartIndex, delEndIndex));
			returnString = returnString.substring(0, delStartIndex) + returnString.substring(delEndIndex);
			//println("result this: " + returnString);
			descChanged = true;
		}
		startIndex = descriptionString.indexOf("/job/", endIndex);
		currentIteration++;
	}
	if (descChanged) {
		thisJob.setDescription(returnString);
		thisJob.save();
	}
	
	return returnString;
}

// ----------------------------------------------------------------------
// 	copyFile
//
//	This copies the config.xml to a temp location for processing
//	This should be changed to an xcopy and then a stream read so the a 
//  file lock is not needed (causing 500 error).
//
// ----------------------------------------------------------------------
	private void copyFile(String source, String destination, String sToolsFolder) {
		byte [] data = null;
		int numBytes = 0;
		try {
			FileInputStream fis = new FileInputStream(source);
			data = new byte[fis.available()];
			fis = new FileInputStream(source);
			numBytes = fis.read(data);
			fis.close();
		} catch (Exception e) {
			println(e);
			e.printStackTrace(System.out);
		}
		
		try {
			File destFile = new File(destination);
			destFile.createNewFile();
			
			FileOutputStream fis = new FileOutputStream(destination);
			fis.write(data);
		} catch (Exception e) {
			println(e);
			e.printStackTrace(System.out);
		}
		
	}

// ----------------------------------------------------------------------
// 	xcopyFile
//
//	This copies the config.xml to a temp location for processing
//	This should be changed to an xcopy and then a stream read so the a 
//  file lock is not needed (causing 500 error).
//
// ----------------------------------------------------------------------
private void xcopyFile(String source, String destination, String sToolsFolder) {

	String command = "COPY /Y \"" + source  + "\" \"" +  destination + "\" > out.txt" ;
	
	println("--------------------------------");
	println("  Copying config.xml to temporary location for processing...");
	println(command);
	println("--------------------------------");
	def proc = null;
	try {
		//Execute the command
		proc = command.execute(null, new File(sToolsFolder)) // Call *execute* on the string
		proc.waitFor() // Wait for the command to finish
		returnCode = "${ proc.exitValue()}";
		failure = false;
	} catch(Exception e) {
		e.printStackTrace(System.out);
	}
}


// ----------------------------------------------------------------------
// 	public byte[] loadData(String fileName)
//
//
// ----------------------------------------------------------------------
public byte[] loadData(String fileName) {
		byte[] returnVal = null;
		try {
			FileInputStream fis = new FileInputStream(fileName);
			returnVal = new byte[fis.available()];
			fis.read(returnVal);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnVal;
	}

// ----------------------------------------------------------------------
// 	Vector<String> getXMLNodes(...)
//
//	Get xml data from the config.xml.
//  Used to get the name and description fields from the Jenkins job.
//
// ----------------------------------------------------------------------
	public Vector<String> getXMLNodes(Document configXML, String[] nodePath, String nodePath2) {
		Vector<String> nodeDatas = new Vector<String>();
		String nodeData = "";
		NodeList childNodes = configXML.getDocumentElement().getChildNodes();
		int pathIndex = 0;
		//for (int i = 1; i < childNodes.getLength(); i+=2) {
		boolean done = false;
		while(!done) {
			done = true;
			for (int i = 1; i < childNodes.getLength(); i+=2) {
				if (childNodes.item(i).getNodeName().equalsIgnoreCase(nodePath[pathIndex])) {
					if (pathIndex == nodePath.length-1) {
						//nodeData = childNodes.item(i).getTextContent();
						//nodeDatas.add(nodeData);
						//check the rest of the child nodes
						for (; i < childNodes.getLength(); i+=2) {
							if (childNodes.item(i).getNodeName().equalsIgnoreCase(nodePath[pathIndex])) {
								NodeList childNodes2 = childNodes.item(i).getChildNodes();
								for (int k = 1; k < childNodes2.getLength(); k+=2) {
									if (childNodes2.item(k).getNodeName().equalsIgnoreCase(nodePath2)) {
										nodeData = childNodes2.item(k).getTextContent();
										nodeDatas.add(nodeData);
									}
								}
							}
						}
						done = true;
					} else {
						childNodes = childNodes.item(i).getChildNodes();
						pathIndex++;
						done = false;
					}
					
					i = childNodes.getLength();
				}
			}
		}
		return nodeDatas;
	}


// ----------------------------------------------------------------------
// 	Document parseXmlFile(...)
//
//	Parse using builder to get DOM representation of the XML file.
//
// ----------------------------------------------------------------------
private Document parseXmlFile(String fileName){
		Document dom = null;
		//get the factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			
			//Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			//parse using builder to get DOM representation of the XML file
			dom = db.parse(fileName);

		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return dom;
	}

/* -------------------------------------------------------------------------------
	getDescriptionDataFromJob(...)	
	
	Read the job config xml and retrieve the parameter description data using xml
	slurper.
	
	This is necessary because with the new version of Jenkins, remotely triggered
	builds no longer send description data with the parameters (they are blank).
----------------------------------------------------------------------------------*/
String getDescriptionDataFromJob(List<String> paramDefsDescr, String strHostname, String strJobName, String strUser, String strPass) {
	println("      --- Parsing string parameters from: [" + strJobName + "] - Start");
	def client = new HttpClient()
	
	client.state.setCredentials(
		new AuthScope(strHostname, 8080 ),
		new UsernamePasswordCredentials(strUser, strPass)
	)
	
	println("      --- Connect to Jenkins config via http");
	client.params.authenticationPreemptive = true
	String strConfigLink = "http://" + strHostname + ":8080/job/" + strJobName + "/config.xml";
	def httpGet = new GetMethod(strConfigLink)
	httpGet.doAuthentication = true
	
	try {
		println("      --- Get config.xml data");

		client.executeMethod(httpGet)
		def consoleTextBuffer = new BufferedReader(new InputStreamReader(httpGet.getResponseBodyAsStream()));
		StringBuilder builder = new StringBuilder();
		
		String aux = "";
		while((aux = consoleTextBuffer.readLine()) != null ) {
			builder.append(aux);
		}
		String consoleText = builder.toString();
		//println("      --- Parse the xml: [" + consoleText.substring(0,30) + "...]");
		def thexml = new XmlSlurper().parseText(consoleText);
				
		thexml.properties.'hudson.model.ParametersDefinitionProperty'.parameterDefinitions.'hudson.model.StringParameterDefinition'.description.each { descriptionTag->			
			String sDescription = descriptionTag.text();
			// Add to the list.
			paramDefsDescr.add(sDescription);
			//println("paramDefsDescr.add(sDescription): [" + sDescription + "]");
		}

	} catch(e) {
		println("!!!Error parsing xml!");
      	println("\t"+e)
	} finally {
		println("   --- Parsed [" + paramDefsDescr.size() + "] parameters from: [" + strJobName + "] - End");
	}
}