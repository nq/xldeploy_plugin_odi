# xld_plugin_odi

This is a plugin to deploy ODI (Oracle Data Intrgrator) "scenarios" and "load plans" by means of
using the ODI Java SDK to import the exported XML files located in 
[this](https://code.paychex.com/projects/BIC/repos/erd_etl/browse) repo. 

## Usage

To build:

```bash
$ ./gradlew idea
```

Or:

```bash
$ ./gradlew eclipse
```

Then:

```bash
$ ./gradlew xldp
```

This will place an `*.xldp` file in the `build/libs` directory. This file should be copied into the plugins directory of
XLD. 

## Miscellaneous Notes

Here are some good resources:

- [Automating ODI development tasks using the SDK](https://blogs.oracle.com/dataintegration/automating-odi-development-tasks-using-the-sdk/comment-submitted?cid=40f587b8-ce20-43d8-afad-b9bd20b5a53c): has a bunch of examples of using the ODI SDK
- [ODI SDK Public API](https://docs.oracle.com/cd/E28280_01/apirefs.1111/e17060/toc.htm): Javadocs for the SDK.