pipeline{
    agent {
        //This used have a restriction for no docker agents and not jknapprw1. They have been removed so totem can build on more agents in the future
        label 'linux && !ensaptstw2 && !dockercertify'
    }
    tools {
        jdk '1.8.0_77_x64-linux'
    }
    options {
        timestamps()
        skipDefaultCheckout()
    }

    stages {
        stage('Checkout SCM') {
            steps {
                cleanWs()
                checkout scm
                step([$class: 'StashNotifier'])
            }
        }

        stage('Build & Test') {
            steps {
                sh './gradlew clean test --refresh-dependencies --stacktrace'
            }
        }

        stage('Deploy') {
            when {
                branch 'master'
            }
            steps {
                withHttpRelay(hostVariableName: 'ARTIFACTORY_HOST', portVariableName: 'ARTIFACTORY_PORT', templateId: '59863fc4-45b9-4c19-b026-5d9a905d235f') {
                    sh './gradlew artifactoryPublish --stacktrace'
                }
            }
        }
    }

    post {
        always {
            script {
                currentBuild.result = currentBuild.result ?: 'SUCCESS'
                step([$class: 'StashNotifier'])
            }
        }
    }
}