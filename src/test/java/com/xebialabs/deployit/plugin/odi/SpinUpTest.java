package com.xebialabs.deployit.plugin.odi;

import com.paychex.eba.xldeploy.spinup.XLDeploySpinUp;
import com.paychex.slac.xldeploy.rest.client.XLDeployClient;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class SpinUpTest {

    @BeforeClass
    public static void boot() {
        XLDeploySpinUp.spinUp();
    }

    @Test
    public void odiScenarioTest() {
        // If this test fails, the the name of the package might not start with com.xebialabs
        XLDeployClient xlDeployClient = new XLDeployClient("http","localhost", XLDeploySpinUp.getPort(), "admin", "admin");
        assertNotNull(xlDeployClient.getMetadataClient().type("odi.Scenario"));
    }

}
