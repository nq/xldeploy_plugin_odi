package com.xebialabs.deployit.plugin.odi.step;

import com.xebialabs.deployit.plugin.api.flow.ExecutionContext;
import com.xebialabs.deployit.plugin.api.flow.Step;
import com.xebialabs.deployit.plugin.api.flow.StepExitCode;
import com.xebialabs.deployit.plugin.odi.ci.DeployedScenario;
import com.xebialabs.deployit.plugin.odi.ci.Server;
import com.xebialabs.overthere.OverthereFile;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.util.XmlSlurper;
import groovy.util.slurpersupport.GPathResult;
import oracle.odi.core.OdiInstance;
import oracle.odi.core.config.MasterRepositoryDbInfo;
import oracle.odi.core.config.OdiInstanceConfig;
import oracle.odi.core.config.PoolingAttributes;
import oracle.odi.core.config.WorkRepositoryDbInfo;
import oracle.odi.core.persistence.IOdiEntityManager;
import oracle.odi.core.persistence.transaction.ITransactionDefinition;
import oracle.odi.core.persistence.transaction.ITransactionManager;
import oracle.odi.core.persistence.transaction.ITransactionStatus;
import oracle.odi.core.persistence.transaction.support.DefaultTransactionDefinition;
import oracle.odi.domain.runtime.scenario.OdiScenario;
import oracle.odi.domain.runtime.scenario.finder.IOdiScenarioFinder;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringJoiner;

@SuppressWarnings("serial")
public class DeleteScenarioStep implements Step {

    private DeployedScenario deployedArtifact;

    public DeleteScenarioStep(DeployedScenario deployedArtifact) {
        this.deployedArtifact = deployedArtifact;

    }

    @Override
    public String getDescription() {
        return "Import an ODI Scenario into an ODI instance";
    }

    @Override
    public StepExitCode execute(ExecutionContext ctx) throws Exception {
        // Connections Details
//       String odiSupervisorUser = "SUPERVISOR";
//       String odiSupervisorPassword = "simple4u";

        Server server = this.deployedArtifact.getContainer();
        OverthereFile artifact = this.deployedArtifact.getFile();

        Path tempDir = Files.createTempDirectory("scenarios");
        Path tempFile = Paths.get(tempDir.toString(), artifact.getName());

        Files.copy(artifact.getInputStream(), tempFile);

        artifact.getInputStream().close();

        String masterRepositoryJdbcUrl = server.getUrl();
        String masterRepositoryJdbcDriver = "oracle.jdbc.OracleDriver";
        String masterRepositoryJdbcUser = server.getUsername();
        String masterRepositoryJdbcPassword = server.getPassword();
        String workRepositoryName = server.getWorkRepositoryName();

        // Respository and ODI Instance
        MasterRepositoryDbInfo masterRepoDbInfo = new MasterRepositoryDbInfo(masterRepositoryJdbcUrl, masterRepositoryJdbcDriver, masterRepositoryJdbcUser, masterRepositoryJdbcPassword.toCharArray(), new PoolingAttributes());
        WorkRepositoryDbInfo workRepoDbInfo = new WorkRepositoryDbInfo(workRepositoryName, new PoolingAttributes());
        OdiInstance odiInstance = OdiInstance.createInstance(new OdiInstanceConfig(masterRepoDbInfo, workRepoDbInfo));

        // Authentication
//       Authentication auth = odiInstance.getSecurityManager().createAuthentication(odiSupervisorUser, odiSupervisorPassword.toCharArray());
//       odiInstance.getSecurityManager().setCurrentThreadAuthentication(auth);

        try {

            // Transaction Instance
            ITransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
            ITransactionManager transactionManager = odiInstance.getTransactionManager();
            ITransactionStatus transactionStatus = transactionManager.getTransaction(transactionDefinition);

            // Project lookup
//            OdiProject sdkProject = ((IOdiProjectFinder) odiInstance.getTransactionalEntityManager().getFinder(OdiProject.class)).findByCode("ODI_SDK_PROJECT");


            GPathResult slurper = new XmlSlurper().parse(tempFile.toFile());

            final Binding binding = new Binding();
            binding.setVariable("slurper", slurper);

            final StringJoiner sj = new StringJoiner(System.getProperty("line.separator"));
            sj.add("slurper.Object.find{ it.@class == \"com.sunopsis.dwg.dbobj.SnpScen\" }.Field.find{ it.@name == \"ScenName\" }.text()");

            String scenarioName = (String)new GroovyShell(binding).evaluate(sj.toString());

            System.out.println("Deleting Scenarios...");

            IOdiEntityManager entityManager = odiInstance.getTransactionalEntityManager();
            OdiScenario scenario = ((IOdiScenarioFinder)entityManager.getFinder(OdiScenario.class)).findLatestByName(scenarioName, true);

            entityManager.remove(scenario);

            transactionManager.commit(transactionStatus);
//           auth.close();
            odiInstance.close();

        } catch (Exception e) {

            // Commit transaction, Close Authentication and ODI Instance in Exception Block
//           auth.close();
            odiInstance.close();
            System.out.println(e);
            return StepExitCode.FAIL;
        }

        return StepExitCode.SUCCESS;
    }


    @Override
    public int getOrder() {
        return 90;
    }
}