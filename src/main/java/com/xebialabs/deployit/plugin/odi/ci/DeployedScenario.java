package com.xebialabs.deployit.plugin.odi.ci;

import com.xebialabs.deployit.plugin.api.deployment.planning.Create;
import com.xebialabs.deployit.plugin.api.deployment.planning.DeploymentPlanningContext;
import com.xebialabs.deployit.plugin.api.deployment.planning.Destroy;
import com.xebialabs.deployit.plugin.api.deployment.planning.Modify;
import com.xebialabs.deployit.plugin.api.udm.base.BaseDeployedArtifact;
import com.xebialabs.deployit.plugin.odi.step.DeleteScenarioStep;
import com.xebialabs.deployit.plugin.odi.step.ImportScenarioStep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeployedScenario extends BaseDeployedArtifact<Scenario, Server> {

    private static final Logger logger = LoggerFactory.getLogger(DeployedScenario.class);

    @Create
    @Modify
    public void deploy(DeploymentPlanningContext result) {
        logger.info("Adding scenario deployment step");
        result.addStep(new ImportScenarioStep(this));
    }

    @Destroy
    public void undeploy(DeploymentPlanningContext result) {
        logger.info("Adding scenario undeployment step");
        result.addStep(new DeleteScenarioStep(this));
    }
}
