package com.xebialabs.deployit.plugin.odi.ci;

import com.xebialabs.deployit.plugin.api.reflect.PropertyDescriptor;
import com.xebialabs.deployit.plugin.api.services.SearchParameters;
import com.xebialabs.deployit.plugin.api.udm.CandidateValuesFilter;
import com.xebialabs.deployit.plugin.api.udm.ConfigurationItem;
import com.xebialabs.deployit.plugin.api.udm.Metadata;
import com.xebialabs.deployit.plugin.api.udm.Property;
import com.xebialabs.deployit.plugin.api.udm.base.BaseDeployableArtifact;

@Metadata(virtual=false, description="This is a DeploymentPackage")
public class Scenario extends BaseDeployableArtifact {

//    @Property(defaultValue = "odi", candidateValuesFilter = "getOdiScenarioType")
    @Property(defaultValue = "odi")
    public String scenarioType;

//    @CandidateValuesFilter(name = "getOdiScenarioType")
//    static public SearchParameters getOdiScenarioType(ConfigurationItem ci, PropertyDescriptor property) {
//
//    }
}

