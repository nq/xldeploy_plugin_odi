package com.xebialabs.deployit.plugin.odi.step;

import com.xebialabs.deployit.plugin.odi.ci.Server;
import com.xebialabs.deployit.plugin.api.flow.ExecutionContext;
import com.xebialabs.deployit.plugin.api.flow.Step;
import com.xebialabs.deployit.plugin.api.flow.StepExitCode;
import oracle.odi.core.OdiInstance;
import oracle.odi.core.config.MasterRepositoryDbInfo;
import oracle.odi.core.config.OdiInstanceConfig;
import oracle.odi.core.config.PoolingAttributes;
import oracle.odi.core.config.WorkRepositoryDbInfo;
import oracle.odi.core.persistence.transaction.ITransactionDefinition;
import oracle.odi.core.persistence.transaction.ITransactionManager;
import oracle.odi.core.persistence.transaction.ITransactionStatus;
import oracle.odi.core.persistence.transaction.support.DefaultTransactionDefinition;

import oracle.odi.domain.project.OdiProject;

import oracle.odi.domain.project.finder.IOdiProjectFinder;
import oracle.odi.impexp.support.ImportServiceImpl;
import oracle.odi.impexp.IImportService;

@SuppressWarnings("serial")
public class ImportLoadPlan implements Step {

    private Server server;

    public ImportLoadPlan(Server server) {
        this.server = server;
    }

    @Override
    public String getDescription() {
        return "Starting " + server;
    }

    @Override
    public StepExitCode execute(ExecutionContext ctx) throws Exception {
        System.out.println(ctx);

//       List<String> params = new LinkedList<>();
//       params.add("-1");
//       params.add("EXADATA_SPIMF_N2");
//       params.add(Utils.formatParameter("AGENT_URL", ctx.getAttribute("agentUrl").toString()));
//       params.add(Utils.formatParameter("ERD.v_deploy_file", ctx.getAttribute("file").toString()));
//       params.add(Utils.formatParameter("ERD.v_deploy_workrep", ctx.getAttribute("workRepository").toString()));
//       params.addAll(server.getPropertiesAsList());
//
//       String[] paramsArray = params.toArray(new String[0]);
//
//       ImportScen importScen = new ImportScen();
//       Vector<String[]> cmdline = SnpsObject.treatCommandLine(paramsArray, '=');
//       importScen.setParameters(cmdline);
//       try {
//           importScen.execute();
//       } catch (Exception e) {
//           System.err.println("Error executing step");
//           e.printStackTrace(System.err);
//           return StepExitCode.FAIL;
//       }


        // Connections Details
//       String odiSupervisorUser = "SUPERVISOR";
//       String odiSupervisorPassword = "simple4u";

        String masterRepositoryJdbcUrl = "jdbc:oracle:thin:@//localhost:1521/XE";
        String masterRepositoryJdbcDriver = "oracle.jdbc.OracleDriver";
        String masterRepositoryJdbcUser = "DEV_ODI_REPO";
        String masterRepositoryJdbcPassword = "simple4u";
        String workRepositoryName = "WORKREP";

        // KM XML reference Path
        String kmInstallFolder = "C:/Oracle/MiddlewareWeblogic/Oracle_ODI/oracledi/xml-reference/";

        // Respository and ODI Instance
        MasterRepositoryDbInfo mRepDbInfo = new MasterRepositoryDbInfo(masterRepositoryJdbcUrl, masterRepositoryJdbcDriver, masterRepositoryJdbcUser, masterRepositoryJdbcPassword.toCharArray(), new PoolingAttributes());
        WorkRepositoryDbInfo wRepDbInfo = new WorkRepositoryDbInfo(workRepositoryName, new PoolingAttributes());
        OdiInstance odiInstance = OdiInstance.createInstance(new OdiInstanceConfig(mRepDbInfo, wRepDbInfo));

        // Authentication
//       Authentication auth = odiInstance.getSecurityManager().createAuthentication(odiSupervisorUser, odiSupervisorPassword.toCharArray());
//       odiInstance.getSecurityManager().setCurrentThreadAuthentication(auth);

        try {

            // Transaction Instance
            ITransactionDefinition txnDef = new DefaultTransactionDefinition();
            ITransactionManager tm = odiInstance.getTransactionManager();
            ITransactionStatus txnStatus = tm.getTransaction(txnDef);

            // Project lookup
            OdiProject sdkProject = ((IOdiProjectFinder) odiInstance.getTransactionalEntityManager().getFinder(OdiProject.class)).findByCode("ODI_SDK_PROJECT");

            System.out.println("Importing Load Plans...");

            // Import KMs
            IImportService impService = new ImportServiceImpl(odiInstance);
            impService.importObjectFromXml(IImportService.IMPORT_MODE_DUPLICATION, kmInstallFolder + "KM_IKM Oracle Incremental Update.xml", sdkProject, false);
            impService.importObjectFromXml(IImportService.IMPORT_MODE_DUPLICATION, kmInstallFolder + "KM_CKM Oracle.xml", sdkProject, false);
            impService.importObjectFromXml(IImportService.IMPORT_MODE_DUPLICATION, kmInstallFolder + "KM_LKM SQL to Oracle.xml", sdkProject, false);

            System.out.println("KMs Imported:");
            System.out.println("KM_IKM Oracle Incremental Update");
            System.out.println("KM_CKM Oracle");
            System.out.println("KM_LKM SQL to Oracle");

            // Commit transaction, Close Aithentication and ODI Instance
            tm.commit(txnStatus);
//           auth.close();
            odiInstance.close();

        } catch (Exception e) {

            // Commit transaction, Close Aithentication and ODI Instance in Exception Block
//           auth.close();
            odiInstance.close();
            System.out.println(e);
        }
       /*
       Usage of shell script:
       startscen.sh <name> <version> <context_code> [<log_level>] [-session_name=<session_name>] [-keywords=<keywords>] [-name=<agent_name>] [-v=<trace_level>] [<variable>=<value>]*"
       Need to run this somehow:
        $ODI_JAVA_START oracle.odi.Agent $ODI_REPOSITORY_PARAMS ODI_START_SCEN "$@"
           ODI_DEPLOY
           -1
           EXADATA_SPIMF_N2
           -AGENT_URL=http://rptdvh3.paychex.com:20930/oraclediagent
           ERD.v_deploy_workrep=WORKREP1
           ERD.v_deploy_file=/var/appl/odi/zipExtract/N2/2018-10-16-10-36-29-847/ODI/load_plans/LP_2018_10_01_QCY_MAJ_REL_WKND.xml
        */
        return StepExitCode.SUCCESS;
    }


    public Server getServer() {
        return server;
    }

    @Override
    public int getOrder() {
        return 90;
    }
}