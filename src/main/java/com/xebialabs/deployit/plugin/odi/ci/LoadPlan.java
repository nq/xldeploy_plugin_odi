package com.xebialabs.deployit.plugin.odi.ci;

import com.xebialabs.deployit.plugin.api.udm.Metadata;
import com.xebialabs.deployit.plugin.api.udm.base.BaseDeployableArtifact;

@Metadata(virtual=false, description="This is a DeploymentPackage")
public class LoadPlan extends BaseDeployableArtifact {
}