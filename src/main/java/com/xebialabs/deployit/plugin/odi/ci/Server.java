package com.xebialabs.deployit.plugin.odi.ci;

import com.xebialabs.deployit.plugin.api.udm.Metadata;
import com.xebialabs.deployit.plugin.api.udm.Property;
import com.xebialabs.deployit.plugin.api.udm.base.BaseContainer;

@Metadata(root = Metadata.ConfigurationItemRoot.INFRASTRUCTURE, virtual = false, description = "This is an Server")
public class Server extends BaseContainer {

	/*
	Things this class needs to take care of:
	  -SECURITY_DRIVER=$ODI_SECU_DRIVER
	  -SECURITY_URL=$ODI_SECU_URL
	  -SECURITY_USER=$ODI_SECU_USER
	  -SECURITY_PWD=$ODI_SECU_ENCODED_PASS
	  -USER=$ODI_USER
	  -PASSWORD=$ODI_ENCODED_PASS
	  -WORK_REP_NAME=$ODI_SECU_WORK_REP

	To do this, I likely need to find out how $ODI_HOME/bin/odiparams.sh works (need access to ODI server)
	 */

	@Property
	private String url;

	@Property
	private String username;

	@Property(password = true)
	private String password;

	@Property(required = false)
	private String securityDriver;

	@Property(required = false)
	private String securityUrl;

	@Property(required = false)
	private String securityUsername;

	@Property(password = true, required = false)
	private String securityPassword;

	@Property
	private String workRepositoryName;

	public String getUrl() {
		return url;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getSecurityDriver() {
		return securityDriver;
	}

	public String getSecurityUrl() {
		return securityUrl;
	}

	public String getSecurityUsername() {
		return securityUsername;
	}

	public String getSecurityPassword() {
		return securityPassword;
	}

	public String getWorkRepositoryName() {
		return workRepositoryName;
	}
}