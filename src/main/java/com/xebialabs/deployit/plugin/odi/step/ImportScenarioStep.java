package com.xebialabs.deployit.plugin.odi.step;

import com.xebialabs.deployit.plugin.odi.ci.DeployedScenario;
import com.xebialabs.deployit.plugin.odi.ci.Server;
import com.xebialabs.deployit.plugin.api.flow.ExecutionContext;
import com.xebialabs.deployit.plugin.api.flow.Step;
import com.xebialabs.deployit.plugin.api.flow.StepExitCode;
import com.xebialabs.overthere.OverthereFile;
import oracle.odi.core.OdiInstance;
import oracle.odi.core.config.MasterRepositoryDbInfo;
import oracle.odi.core.config.OdiInstanceConfig;
import oracle.odi.core.config.PoolingAttributes;
import oracle.odi.core.config.WorkRepositoryDbInfo;
import oracle.odi.core.persistence.transaction.ITransactionDefinition;
import oracle.odi.core.persistence.transaction.ITransactionManager;
import oracle.odi.core.persistence.transaction.ITransactionStatus;
import oracle.odi.core.persistence.transaction.support.DefaultTransactionDefinition;
import oracle.odi.impexp.IImportService;
import oracle.odi.impexp.support.ImportServiceImpl;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@SuppressWarnings("serial")
public class ImportScenarioStep implements Step {

    private DeployedScenario deployedArtifact;

    public ImportScenarioStep(DeployedScenario deployedArtifact) {
        this.deployedArtifact = deployedArtifact;

    }

    @Override
    public String getDescription() {
        return "Import an ODI Scenario into an ODI instance";
    }

    @Override
    public StepExitCode execute(ExecutionContext ctx) throws Exception {
        // Connections Details
//       String odiSupervisorUser = "SUPERVISOR";
//       String odiSupervisorPassword = "simple4u";

        Server server = this.deployedArtifact.getContainer();
        OverthereFile artifact = this.deployedArtifact.getFile();

        Path tempDir = Files.createTempDirectory("scenarios");
        Path tempFile = Paths.get(tempDir.toString(), artifact.getName());

        Files.copy(artifact.getInputStream(), tempFile);

        artifact.getInputStream().close();

        String masterRepositoryJdbcUrl = server.getUrl();
        String masterRepositoryJdbcDriver = "oracle.jdbc.OracleDriver";
        String masterRepositoryJdbcUser = server.getUsername();
        String masterRepositoryJdbcPassword = server.getPassword();
        String workRepositoryName = server.getWorkRepositoryName();

        // Respository and ODI Instance
        MasterRepositoryDbInfo masterRepoDbInfo = new MasterRepositoryDbInfo(masterRepositoryJdbcUrl, masterRepositoryJdbcDriver, masterRepositoryJdbcUser, masterRepositoryJdbcPassword.toCharArray(), new PoolingAttributes());
        WorkRepositoryDbInfo workRepoDbInfo = new WorkRepositoryDbInfo(workRepositoryName, new PoolingAttributes());
        OdiInstance odiInstance = OdiInstance.createInstance(new OdiInstanceConfig(masterRepoDbInfo, workRepoDbInfo));

        // Authentication
//       Authentication auth = odiInstance.getSecurityManager().createAuthentication(odiSupervisorUser, odiSupervisorPassword.toCharArray());
//       odiInstance.getSecurityManager().setCurrentThreadAuthentication(auth);

        try {

            // Transaction Instance
            ITransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
            ITransactionManager transactionManager = odiInstance.getTransactionManager();
            ITransactionStatus transactionStatus = transactionManager.getTransaction(transactionDefinition);

            // Project lookup
//            OdiProject sdkProject = ((IOdiProjectFinder) odiInstance.getTransactionalEntityManager().getFinder(OdiProject.class)).findByCode("ODI_SDK_PROJECT");

            System.out.println("Importing Scenarios...");

            // Import KMs
            IImportService impService = new ImportServiceImpl(odiInstance);
            impService.importObjectFromXml(IImportService.IMPORT_MODE_SYNONYM_INSERT_UPDATE, tempFile.toAbsolutePath().toString(), false);

            // Commit transaction, Close Aithentication and ODI Instance
            transactionManager.commit(transactionStatus);
//           auth.close();
            odiInstance.close();

        } catch (Exception e) {

            // Commit transaction, Close Authentication and ODI Instance in Exception Block
//           auth.close();
            odiInstance.close();
            System.out.println(e);
            return StepExitCode.FAIL;
        }

        return StepExitCode.SUCCESS;

       /*
       Need to run this somehow:
        $ODI_JAVA_START com.sunopsis.dwg.tools."$@"
          -SECURITY_DRIVER=$ODI_SECU_DRIVER
          -SECURITY_URL=$ODI_SECU_URL
          -SECURITY_USER=$ODI_SECU_USER
          -SECURITY_PWD=$ODI_SECU_ENCODED_PASS
          -USER=$ODI_USER
          -PASSWORD=$ODI_ENCODED_PASS
          -WORK_REP_NAME=$ODI_SECU_WORK_REP
          -FILE_NAME=/var/appl/odi/zipExtract/N2/2018-10-16-10-36-29-847/ODI/scenarios/SCEN_BNCHMRK_LOAD_001.xml
          -IMPORT_MODE=SYNONYM_INSERT_UPDATE
        */

    }


    @Override
    public int getOrder() {
        return 90;
    }
}