@echo off

:: Enables Delayed Variable Expansion...
SETLOCAL ENABLEDELAYEDEXPANSION

echo -------------------------------------------
echo ------ EBA Deployment script - START ------
echo -------------------------------------------
if "%ADM_Stage%"=="true" goto ADM_Trigger
if "%ADM_Stage%"=="TRUE" goto ADM_Trigger
echo.
echo       - Create folder on deployment server
echo       - Delete contents of deployment folder - only if specified.
echo       - Copy artifacts to deployment folder
echo.
::       Rules...
::       - No spaces around "=" when assigning variables.
::       - Server_X can be pngdep00, pngdep01, pngdep02 or any windows share.
::       - Quotes are not necessary - but allowed.
:: **************************************************************************
:: Overrides
::   These values can be set from the job to implement custom behavior.
::   Otherwise, they will receive default values below.
:: **************************************************************************
echo --- Setting and verifying paths ---
::      Note: if there is a syntax error here - check the defined paths carefuly for
::		for any quotes or misc characters.
if "%Server_Dev%"=="" set Server_Dev=pngdep00
if "%Server_N%"=="" set Server_N=pngdep01
if "%Server_Production%"=="" set Server_Production=pngdep02
if "%Path_prefix%"=="" set Path_prefix=/usr/local/pngbuilds/work/app
:: For old-style jobs, allow the automatic setting of all paths based on single environment variable
:: To do this, set the [Path_All] variable with the appropriate staging environment variable.
if "%Path_ALL%" EQU "" goto End_PathSet
echo        --- Path_ALL variable detected, Auto-filling default paths - for old-style jobs - ---
if "%Path_Devsh1%"=="" set Path_Devsh1=%Path_ALL%
if "%Path_Devsh2%"=="" set Path_Devsh2=%Path_ALL%
if "%Path_Devsh3%"=="" set Path_Devsh3=%Path_ALL%
if "%Path_Devsh4%"=="" set Path_Devsh4=%Path_ALL%
if "%Path_devalt%"=="" set Path_devalt=%Path_ALL%
if "%Path_dvs2%"=="" set Path_dvs2=%Path_ALL%
if "%Path_dvs3%"=="" set Path_dvs3=%Path_ALL%
if "%Path_dvs4%"=="" set Path_dvs4=%Path_ALL%
if "%Path_dev%"=="" set Path_dev=%Path_ALL%
if "%Path_dev2%"=="" set Path_dev2=%Path_ALL%
if "%Path_N2%"=="" set Path_N2=%Path_ALL%
if "%Path_N2A%"=="" set Path_N2A=%Path_ALL%
if "%Path_N2B%"=="" set Path_N2B=%Path_ALL%
if "%Path_N2C%"=="" set Path_N2C=%Path_ALL%
if "%Path_N2D%"=="" set Path_N2D=%Path_ALL%
if "%Path_N1%"=="" set Path_N1=%Path_ALL%
if "%Path_N0%"=="" set Path_N0=%Path_ALL%
if "%Path_N%"=="" set Path_N=%Path_ALL%
if "%Path_Perf%"=="" set Path_Perf=%Path_ALL%
if "%Path_Prod%"=="" set Path_Prod=%Path_ALL%
if "%Path_OPS%"=="" set Path_OPS=%Path_ALL%
if "%Path_OPS_Mobility%"=="" set Path_OPS_Mobility=%Path_ALL%
if "%Path_N2_Mobility%"=="" set Path_N2_Mobility=%Path_ALL%
if "%Path_N1_Mobility%"=="" set Path_N1_Mobility=%Path_ALL%
if "%Path_N_Mobility%"=="" set Path_N_Mobility=%Path_ALL%
if "%Path_Mobility%"=="" set Path_Mobility=%Path_ALL%
if "%Path_Production%"=="" set Path_Production=%Path_ALL%
:End_PathSet
::
:: ** If the paths below are not set (job using new groovy method) set to the root of pngdep servers.
if "%Path_Devsh1%"=="" set Path_Devsh1=%Path_prefix%/
if "%Path_Devsh2%"=="" set Path_Devsh2=%Path_prefix%/
if "%Path_Devsh3%"=="" set Path_Devsh3=%Path_prefix%/
if "%Path_Devsh4%"=="" set Path_Devsh4=%Path_prefix%/
if "%Path_Devsh4%"=="" set Path_Devsh4=%Path_prefix%/
if "%Path_devalt%"=="" set Path_devalt=%Path_prefix%/
if "%Path_dvs2%"=="" set Path_dvs2=%Path_prefix%/
if "%Path_dvs3%"=="" set Path_dvs3=%Path_prefix%/
if "%Path_dvs4%"=="" set Path_dvs4=%Path_prefix%/
if "%Path_dev%"=="" set Path_dev=%Path_prefix%/
if "%Path_dev2%"=="" set Path_dev2=%Path_prefix%/
if "%Path_N2%"=="" set Path_N2=%Path_prefix%/
if "%Path_N2A%"=="" set Path_N2A=%Path_prefix%/
if "%Path_N2B%"=="" set Path_N2B=%Path_prefix%/
if "%Path_N2C%"=="" set Path_N2C=%Path_prefix%/
if "%Path_N2D%"=="" set Path_N2D=%Path_prefix%/
if "%Path_N1%"=="" set Path_N1=%Path_prefix%/
if "%Path_N0%"=="" set Path_N0=%Path_prefix%/
if "%Path_N%"=="" set Path_N=%Path_prefix%/
if "%Path_Perf%"=="" set Path_Perf=%Path_prefix%/
if "%Path_Prod%"=="" set Path_Prod=%Path_prefix%/
if "%Path_OPS%"=="" set Path_OPS=%Path_prefix%/
if "%Path_OPS_Mobility%"=="" set Path_OPS_Mobility=%Path_prefix%/
if "%Path_N2_Mobility%"=="" set Path_N2_Mobility=%Path_prefix%/
if "%Path_N1_Mobility%"=="" set Path_N1_Mobility=%Path_prefix%/
if "%Path_N_Mobility%"=="" set Path_N_Mobility=%Path_prefix%/
if "%Path_Mobility%"=="" set Path_Mobility=%Path_prefix%/
if "%Path_Production%"=="" set Path_Production=%Path_prefix%/
if "%Path_eba_stage%"=="" set Path_eba_stage=%Path_prefix%/eba_stage/%JOB_NAME%
if "%Path_SourceFiles%"=="" set Path_SourceFiles=%WORKSPACE%\Staging\*.*
:: ** If the job is using the old label, assume unix for the folder contents delete.
if "%DELETE_FOLDER_CONTENTS%" NEQ "" set DELETE_FOLDER_CONTENTS_unix=%DELETE_FOLDER_CONTENTS%
if "%DELETE_FOLDER_CONTENTS2%" NEQ "" set DELETE_FOLDER_CONTENTS_unix2=%DELETE_FOLDER_CONTENTS2%
if "%DELETE_FOLDER_CONTENTS3%" NEQ "" set DELETE_FOLDER_CONTENTS_unix3=%DELETE_FOLDER_CONTENTS3%
if "%DELETE_FOLDER_CONTENTS4%" NEQ "" set DELETE_FOLDER_CONTENTS_unix4=%DELETE_FOLDER_CONTENTS4%
:: ** Allow for the case where lower-case true was specified.
if "%DEBUG_stage_ONLY%"=="true" set DEBUG_stage_ONLY=TRUE
:: ** Check for the use of alternate names for "DeployEnvironment" **
if "%DeployEnvironment%" NEQ "" goto OK_DeployEnvironment
set DeployEnvironment=%Environment%
if "%DeployEnvironment%" NEQ "" goto OK_DeployEnvironment
set DeployEnvironment=%DeploymentEnvironment%
if "%DeployEnvironment%" NEQ "" goto OK_DeployEnvironment
set DeployEnvironment=%StagingEnvironment%
if "%DeployEnvironment%" NEQ "" goto OK_DeployEnvironment
:: If the deployment environment is not set, we cannot continue.
:: This is because we determine the server name from the environment.
echo **************************************************************************
echo ** ERRROR - DeployEnvironment is NOT SET.
echo **    - Check job parameters!
echo **************************************************************************
exit -1
:OK_DeployEnvironment

:: **************************************************************************
:: ** Set the drop location and drop server based on the DeployEnvironment.
:: **************************************************************************
set ServerName=%Server_N%
:: **************************************************************************
:: ** Display message if debug build is detected 
::         - this will be picked up by the post groovy as well
:: **************************************************************************
if "%DEBUG_stage_ONLY%" NEQ "TRUE" goto Non_DebugBuild
echo ***************************************************
echo ***************************************************
echo ************** THIS IS A TEST BUILD ***************
echo ***************************************************
echo ***************************************************
:Non_DebugBuild
echo       Required Variables...
echo       - Environment = [%DeployEnvironment%]
:: Based on the specified deployment environment, assign the appropriate path parameter.
goto CASE_%DeployEnvironment%
		set DropLocation=
		goto END_SWITCH
:CASE_Devsh
    set DropLocation=%Path_Devsh%
    goto END_SWITCH
:CASE_Devsh1
    set DropLocation=%Path_Devsh1%
    set ServerName=%Server_Dev%
    goto END_SWITCH
:CASE_Devsh2
    set DropLocation=%Path_Devsh2%
    set ServerName=%Server_Dev%
    goto END_SWITCH
:CASE_Devsh3
    set DropLocation=%Path_Devsh3%
    set ServerName=%Server_Dev%
    goto END_SWITCH
:CASE_Devsh4
    set DropLocation=%Path_Devsh4%
    set ServerName=%Server_Dev%
    goto END_SWITCH
:CASE_devalt
    set DropLocation=%Path_devalt%
    set ServerName=%Server_Dev%
    goto END_SWITCH
:CASE_dvs2
    set DropLocation=%Path_dvs2%
    set ServerName=%Server_Dev%
    goto END_SWITCH
:CASE_dvs3
    set DropLocation=%Path_dvs3%
    set ServerName=%Server_Dev%
    goto END_SWITCH
:CASE_dvs4
    set DropLocation=%Path_dvs4%
    set ServerName=%Server_Dev%
    goto END_SWITCH
:CASE_dev
    set DropLocation=%Path_dev%
    set ServerName=%Server_Dev%
    goto END_SWITCH
:CASE_dev2
    set DropLocation=%Path_dev2%
    set ServerName=%Server_Dev%
    goto END_SWITCH
:CASE_Perf
    set DropLocation=%Path_Perf%
    goto END_SWITCH
:CASE_N2
    set DropLocation=%Path_N2%
    goto END_SWITCH
:CASE_N2A
    set DropLocation=%Path_N2A%
    goto END_SWITCH
:CASE_N2B
    set ServerName=pngdep00
    set DropLocation=%Path_N2B%
    goto END_SWITCH
:CASE_N2C
    set ServerName=pngdep00
    set DropLocation=%Path_N2C%
    goto END_SWITCH
:CASE_N2D
	set ServerName=pngdep00
    set DropLocation=%Path_N2D%
    goto END_SWITCH
:CASE_N1
    set DropLocation=%Path_N1%
    goto END_SWITCH
:CASE_N0
    set DropLocation=%Path_N0%
    goto END_SWITCH
:CASE_N
    set DropLocation=%Path_N%
    goto END_SWITCH
:CASE_Production
:CASE_Prod
    set DropLocation=%Path_Production%
    set ServerName=%Server_Production%
    goto END_SWITCH
:CASE_eba_stage
    set DropLocation=%Path_eba_stage%
    goto END_SWITCH
:CASE_ADM
    set DropLocation=%Path_ADM%
    set ServerName=ispwd13\bsadeploy
    goto END_SWITCH
:CASE_Virtual
    set DropLocation=%Path_Virtual%
    goto END_SWITCH
:CASE_OPS
    set DropLocation=%Path_OPS%
    goto END_SWITCH
:CASE_OPS_Mobility
    set DropLocation=%Path_OPS_Mobility%
    goto END_SWITCH
:CASE_N2_Mobility
    set DropLocation=%Path_N2_Mobility%
    goto END_SWITCH
:CASE_N1_Mobility
    set DropLocation=%Path_N1_Mobility%
    goto END_SWITCH
:CASE_N_Mobility
    set DropLocation=%Path_N_Mobility%
    goto END_SWITCH
:CASE_Mobility
    set DropLocation=%Path_Mobility%
    set ServerName=%Server_Production%	
    goto END_SWITCH
:END_SWITCH

:: **************************************************************************
:: *** Strip quotes from variables ***
:: **************************************************************************
SET ServerName=%ServerName:"=%
SET DropLocation=%DropLocation:"=%
SET Path_SourceFiles=%Path_SourceFiles:"=%

:: Do some reporting
echo       - ServerName = [%ServerName%]
echo       - DropLocation = [%DropLocation%]
echo       - Source Path = [%Path_SourceFiles%]
echo --------------------------------------------------------------------------
:: At this point, we need the following items to be set - if not, fail.
if "%ServerName%" EQU "" goto Varables_not_set
if "%DropLocation%" EQU "" goto Varables_not_set
if "%Path_SourceFiles%" EQU "" goto Varables_not_set
::
:: Display the contents of all folders to be copied.
echo -- Listing all files in source folder (files to be copied) - START ---
echo.
dir %Path_SourceFiles% /s
echo -- Listing all files in source folder (files to be copied) - END ---

:: **************************************************************************
:: ** If server name starts with "pngdep", use WinSCP.  If not, just use xcopy.
:: **************************************************************************
set server_prefix=%ServerName:~0,6%
if "%server_prefix%" NEQ "pngdep" goto UseXcopy
:UseWinSCP
:: **************************************************************************
:: ** WinSCP **
:: **************************************************************************
:: ** Change all "\" to "/".
set IsWindowsPath=FALSE
set DropLocation=%DropLocation:\=/%
:: *** Strip slashes from server name ***
SET ServerName=%ServerName:/=%
SET ServerName=%ServerName:\=%
::
:: ** If this is a debug build, embed the path with eba_stage.
if "%DEBUG_stage_ONLY%" NEQ "TRUE" goto WinSCP_NonDebug
:: This depends on enabling delayed expansion, which is at the top of this file.
set DropLocation=!DropLocation:%Path_prefix%=%Path_prefix%/eba_stage!

:: The line immediately below ONLY works for /usr/local/pngbuilds/work/app.  Keeping around temporarily.
REM set DropLocation=%DropLocation:pngbuilds/work/app=pngbuilds/work/app/eba_stage%

:: Note: Do the "eba_stage" substiturion below only if DELETE_FOLDER_CONTENTS_unix, DELETE_FOLDER_CONTENTS_unix2 is not empty
::   Otherwise the variable will be set
if "%DELETE_FOLDER_CONTENTS_unix%" NEQ "" set DELETE_FOLDER_CONTENTS_unix=%DELETE_FOLDER_CONTENTS_unix:pngbuilds/work/app=pngbuilds/work/app/eba_stage%
if "%DELETE_FOLDER_CONTENTS_unix2%" NEQ "" set DELETE_FOLDER_CONTENTS_unix2=%DELETE_FOLDER_CONTENTS_unix2:pngbuilds/work/app=pngbuilds/work/app/eba_stage%
if "%DELETE_FOLDER_CONTENTS_unix3%" NEQ "" set DELETE_FOLDER_CONTENTS_unix3=%DELETE_FOLDER_CONTENTS_unix3:pngbuilds/work/app=pngbuilds/work/app/eba_stage%
if "%DELETE_FOLDER_CONTENTS_unix4%" NEQ "" set DELETE_FOLDER_CONTENTS_unix4=%DELETE_FOLDER_CONTENTS_unix4:pngbuilds/work/app=pngbuilds/work/app/eba_stage%
:WinSCP_NonDebug
:: **     Enclose path with "/" if missing.
if "%DropLocation:~-1%" NEQ "/" set DropLocation=%DropLocation%/
if "%DropLocation:~0,1%" NEQ "/" set DropLocation=/%DropLocation%
::
:: ** Check to be sure that the source folder (in the job workspace) is not empty.
:: ** If it is, quit.
for /F %%i in ('dir /b "%Path_SourceFiles%*.*"') do (
   goto :NotEmpty
)
echo *** Source Folder Empty - ignoring file copy ***
goto END_SCRIPT
:NotEmpty
:: If we got here, we have files to copy - so continue.
:CREATE_FOLDER
:: There is a big issue with WinSCP not being able to create folder trees that do not exist.
:: We work around this here - we create a empty folder tree in the workspace and then copy it 
:: to the unix server, thereby creating all the needed folders.
::
:: *** Skip the folder creation if we are using new style copy artifacts.
REM if "%DropLocation%"=="%Path_prefix%/eba_stage/" goto CREATE_FOLDER_DONE
REM if "%DropLocation%"=="%Path_prefix%/" goto CREATE_FOLDER_DONE
echo --------------------------------------------------------------------------
echo    CREATE Drop Location Folder - Using SCP
echo       - Create the deployment folder on pngdep00, pngdep01, or pngdep02
echo       - Folder: [%DropLocation%]
echo --------------------------------------------------------------------------
:: **************************************************************************
:: Create a empty local folder tree (in job) that models the target location on the server
::   Note: This tree contains no files and is not archived.
:: **************************************************************************
set Workspace_Folder_Tree=%WORKSPACE%\%DropLocation%
set Workspace_Folder_Tree=%Workspace_Folder_Tree:/=\%
echo mkdir %Workspace_Folder_Tree%
mkdir %Workspace_Folder_Tree%
:: Create an explicit WINDOWS source path for WinSCP to use when copying.
set Workspace_Folder_Root=%WORKSPACE%%Path_prefix%/*.*
:: Convert slashes to back-slashes to be fully Windows complient.
set Workspace_Folder_Root=%Workspace_Folder_Root:/=\%
:: **************************************************************************
:: Call WinSCP to copy the empty folder tree to the server (thereby creating it on the server)
::   Note: If it already exists, that is fine, it has no effect
:: **************************************************************************
:: Bernie added this to get through the night 3/10/2014
echo %TOOLS_FOLDER%\WinSCP\winscp.com /script=%TOOLS_FOLDER%\WinSCP\Script\pngdep_copy.winscp /parameter %ServerName% %Path_prefix%/ %Workspace_Folder_Root%
%TOOLS_FOLDER%\WinSCP\winscp.com /script=%TOOLS_FOLDER%\WinSCP\Script\pngdep_copy.winscp /parameter %ServerName% %Path_prefix%/ %Workspace_Folder_Root%
if %ERRORLEVEL% NEQ 0 SET ERRORLEV=0
:CREATE_FOLDER_DONE
:: **************************************************************************
:: Delete the folder contents (if explicitly requested by the job)
::   Note: "Delete_Folder_Contents.bat" is called to do the SAFE delete.
:: **************************************************************************
:CLEAN_FOLDER_CONTENTS
:: If the variable is not set - then just go do the file copy.
if "%DELETE_FOLDER_CONTENTS_unix%" EQU "" goto CLEAN_FOLDER_CONTENTS2
echo.
echo.
echo *** calling Delete_Folder_Contents.bat ***
echo %TOOLS_FOLDER%\Script\Delete_Folder_Contents %DELETE_FOLDER_CONTENTS_unix%
call %TOOLS_FOLDER%\Script\Delete_Folder_Contents %DELETE_FOLDER_CONTENTS_unix%
:CLEAN_FOLDER_CONTENTS2
:: If the variable is not set - then just go do the file copy.
if "%DELETE_FOLDER_CONTENTS_unix2%" EQU "" goto CLEAN_FOLDER_CONTENTS3
echo.
echo.
echo *** calling Delete_Folder_Contents.bat - second folder specified ***
echo %TOOLS_FOLDER%\Script\Delete_Folder_Contents %DELETE_FOLDER_CONTENTS_unix2%
call %TOOLS_FOLDER%\Script\Delete_Folder_Contents %DELETE_FOLDER_CONTENTS_unix2%
:CLEAN_FOLDER_CONTENTS3
:: If the variable is not set - then just go do the file copy.
if "%DELETE_FOLDER_CONTENTS_unix3%" EQU "" goto CLEAN_FOLDER_CONTENTS4
echo.
echo.
echo *** calling Delete_Folder_Contents.bat - second folder specified ***
echo %TOOLS_FOLDER%\Script\Delete_Folder_Contents %DELETE_FOLDER_CONTENTS_unix3%
call %TOOLS_FOLDER%\Script\Delete_Folder_Contents %DELETE_FOLDER_CONTENTS_unix3%
:CLEAN_FOLDER_CONTENTS4
:: If the variable is not set - then just go do the file copy.
if "%DELETE_FOLDER_CONTENTS_unix4%" EQU "" goto COPY_ARTIFACTS
echo.
echo.
echo *** calling Delete_Folder_Contents.bat - second folder specified ***
echo %TOOLS_FOLDER%\Script\Delete_Folder_Contents %DELETE_FOLDER_CONTENTS_unix4%
call %TOOLS_FOLDER%\Script\Delete_Folder_Contents %DELETE_FOLDER_CONTENTS_unix4%
:COPY_ARTIFACTS
echo --------------------------------------------------------------------------
echo    Copy Artifacts - using SCP (for pngdep00, pngdep01, and pngdep02 only)
echo --------------------------------------------------------------------------
:: Bernie changed \\build to %tools_folder% 3/10/2014
echo %TOOLS_FOLDER%\WinSCP\winscp.com /script=%TOOLS_FOLDER%\WinSCP\Script\pngdep_copy.winscp /parameter %ServerName% %DropLocation% %Path_SourceFiles%
%TOOLS_FOLDER%\WinSCP\winscp.com /script=%TOOLS_FOLDER%\WinSCP\Script\pngdep_copy.winscp /parameter %ServerName% %DropLocation% %Path_SourceFiles%
goto END_SCRIPT

:: **************************************************************************
:: ** XCOPY -- Necessary folders are created automatically (unlike scp)
:: **************************************************************************
:UseXcopy
set IsWindowsPath=TRUE
:: If this is a debug build, then add the eba_stage to the beginning of the path.
if "%DEBUG_stage_ONLY%" NEQ "TRUE" goto Xcopy_NonDebug
set EBA_STAGE_PATH=\eba_stage
:Xcopy_NonDebug
echo    Copy Artifacts - using xcopy (for all non-pngdep servers)
echo --------------------------------------------------------------------------
:: **************************************************************************
:: ** Change all "/" to "\".
:: **************************************************************************
set DropLocation=%DropLocation:/=\%
:: **************************************************************************
:: Enclose path with "\" if missing.
:: **************************************************************************
if "%DropLocation:~-1%" NEQ "\" set DropLocation=%DropLocation%\
if "%DropLocation:~0,1%" NEQ "\" set DropLocation=\%DropLocation%
:: See if the job wants the folder cleaned first.
if "%DELETE_FOLDER_CONTENTS_Windows%" EQU "" goto Do_XCopy
set DELETE_FOLDER_CONTENTS_Windows=%DELETE_FOLDER_CONTENTS_Windows:/=\%
if "%DELETE_FOLDER_CONTENTS_Windows:~-1%" NEQ "\" set DELETE_FOLDER_CONTENTS_Windows=%DELETE_FOLDER_CONTENTS_Windows%\
if "%DELETE_FOLDER_CONTENTS_Windows:~0,1%" NEQ "\" set DELETE_FOLDER_CONTENTS_Windows=\%DELETE_FOLDER_CONTENTS_Windows%
echo.
echo *** calling Delete_Folder_Contents.bat ***
echo %TOOLS_FOLDER%\Script\Delete_Folder_Contents %EBA_STAGE_PATH%%DELETE_FOLDER_CONTENTS_Windows%
call %TOOLS_FOLDER%\Script\Delete_Folder_Contents %EBA_STAGE_PATH%%DELETE_FOLDER_CONTENTS_Windows%
:Do_XCopy
echo xcopy /Y /F /S %Path_SourceFiles% \\%ServerName%%EBA_STAGE_PATH%%DropLocation%
xcopy /Y /F /S %Path_SourceFiles% \\%ServerName%%EBA_STAGE_PATH%%DropLocation%

:END_SCRIPT
echo.
echo -------------------------------------------
echo ------ EBA Deployment script - END ------
echo -------------------------------------------
exit

:Varables_not_set
echo **************************************************************************
echo    ERROR - One or more required variables is empty.
echo **************************************************************************
exit -1
:ADM_Trigger
echo.
echo ------ ADM - Trigger ------
echo.
echo       - Environment = [ADM]
echo     [ No files will be copied ]
echo    [ ADM will now be triggered ]
echo %TOOLS_FOLDER%\Script\adm_trigger.bat http://build.paychex.com:8080/view/Staging/view/All/job/%JOB_NAME%/lastSuccessfulBuild/artifact/*zip*/archive.zip
%TOOLS_FOLDER%\Script\adm_trigger.bat http://build.paychex.com:8080/view/Staging/view/All/job/%JOB_NAME%/lastSuccessfulBuild/artifact/*zip*/archive.zip
goto END_SCRIPT